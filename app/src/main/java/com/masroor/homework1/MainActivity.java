package com.masroor.homework1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //final,proj,mid1,2,assign              the sequence

    static final String FINAL_WEIGHTAGE="final_weightage";
    static final String PROJECT_WEIGHTAGE="project_weightage";
    static final String MID1_WEIGHTAGE="mid1_weightage";
    static final String MID2_WEIGHTAGE="mid2_weightage";
    static final String ASSIGNMENTS_WEIGHTAGE="assignments_weightage";

    static final String FINAL_MARKS="final_marks";
    static final String PROJECT_MARKS="project_marks";
    static final String MID1_MARKS="mid1_marks";
    static final String MID2_MARKS="mid2_marks";
    static final String ASSIGNMENTS_MARKS="assignments_marks";

    private int final_weightage; float final_marks;
    private int project_weightage; float project_marks;
    private int mid1_weightage; float mid1_marks;
    private int mid2_weightage; float mid2_marks;
    private int assignments_weightage; float assignments_marks;

    static final int REQ_CODE_Weightages=234;
    static final int REQ_CODE_Assessment_Summary=235;
    static final int REQ_CODE_Progress_Summary=236;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void method(View v){
        int id=v.getId();
        switch (id){
            case R.id.weightagesBtn:{
                Intent i=new Intent(this,Weightages.class);
                startActivityForResult(i,REQ_CODE_Weightages);
            }
            break;
            case R.id.assessmentDetailsBtn:{
                Intent i=new Intent(this,Assesments.class);
                //passing the weightages to assessments screen
                i.putExtra(FINAL_WEIGHTAGE,final_weightage);
                i.putExtra(PROJECT_WEIGHTAGE,project_weightage);
                i.putExtra(MID1_WEIGHTAGE,mid1_weightage);
                i.putExtra(MID2_WEIGHTAGE,mid2_weightage);
                i.putExtra(ASSIGNMENTS_WEIGHTAGE,assignments_weightage);
                startActivityForResult(i,REQ_CODE_Assessment_Summary);
            }
            break;
            case R.id.progressSummaryBtn:{
                Intent i=new Intent(this,ProgressSummary.class);
                if((final_weightage+project_weightage+mid1_weightage+mid2_weightage+assignments_weightage)==100){
                    i.putExtra(FINAL_MARKS,final_marks);
                        i.putExtra(FINAL_WEIGHTAGE,final_weightage);
                    i.putExtra(PROJECT_MARKS,project_marks);
                        i.putExtra(PROJECT_WEIGHTAGE,project_weightage);
                    i.putExtra(MID1_MARKS,mid1_marks);
                        i.putExtra(MID1_WEIGHTAGE,mid1_weightage);
                    i.putExtra(MID2_MARKS,mid2_marks);
                        i.putExtra(MID2_WEIGHTAGE,mid2_weightage);
                    i.putExtra(ASSIGNMENTS_MARKS,assignments_marks);
                        i.putExtra(ASSIGNMENTS_WEIGHTAGE,assignments_weightage);

                    startActivityForResult(i,REQ_CODE_Progress_Summary);
                }else{
                    Toast.makeText(this,"You need to specify the weigtages and assessments first.",Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode== Activity.RESULT_OK){
            switch (requestCode){
                case REQ_CODE_Weightages:{

                    final_weightage=(int)data.getExtras().get(FINAL_WEIGHTAGE);
                    project_weightage=(int)data.getExtras().get(PROJECT_WEIGHTAGE);
                    mid1_weightage=(int)data.getExtras().get(MID1_WEIGHTAGE);
                    mid2_weightage=(int)data.getExtras().get(MID2_WEIGHTAGE);
                    assignments_weightage=(int)data.getExtras().get(ASSIGNMENTS_WEIGHTAGE);

                }
                break;
                case REQ_CODE_Assessment_Summary:{

                    final_marks=(float)data.getExtras().get(FINAL_MARKS);
                    project_marks=(float)data.getExtras().get(PROJECT_MARKS);
                    mid1_marks=(float)data.getExtras().get(MID1_MARKS);
                    mid2_marks=(float)data.getExtras().get(MID2_MARKS);
                    assignments_marks=(float)data.getExtras().get(ASSIGNMENTS_MARKS);
                }
                break;
                case REQ_CODE_Progress_Summary:{

                }
                break;
            }
        }
    }
}

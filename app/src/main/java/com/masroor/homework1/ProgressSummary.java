package com.masroor.homework1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ProgressSummary extends AppCompatActivity {

    TextView finalMarks,projectMarks,mid1Marks,mid2Marks,assignMarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_summary);
        Intent i=getIntent();
        finalMarks=findViewById(R.id.f);
        projectMarks=findViewById(R.id.proj);
        mid1Marks=findViewById(R.id.m1);    mid2Marks=findViewById(R.id.m2);
        assignMarks=findViewById(R.id.assign);
        float final_marks=(float)i.getExtras().get(MainActivity.FINAL_MARKS);  int final_weightage=(int)i.getExtras().get(MainActivity.FINAL_WEIGHTAGE);
        float proj_marks=(float)i.getExtras().get(MainActivity.PROJECT_MARKS); int project_weightage=(int)i.getExtras().get(MainActivity.PROJECT_WEIGHTAGE);
        float mid1_marks=(float)i.getExtras().get(MainActivity.MID1_MARKS);    int mid1_weightage=(int)i.getExtras().get(MainActivity.MID1_WEIGHTAGE);
        float mid2_marks=(float)i.getExtras().get(MainActivity.MID2_MARKS);    int mid2_weightage=(int)i.getExtras().get(MainActivity.MID2_WEIGHTAGE);
        float assignments_marks=(float)i.getExtras().get(MainActivity.ASSIGNMENTS_MARKS);  int assignments_weightage=(int)i.getExtras().get(MainActivity.ASSIGNMENTS_WEIGHTAGE);

        Log.i("PASSED","SUCCESS in  getting marks and weights");

        String f=final_marks+"/"+final_weightage,   p=proj_marks+"/"+project_weightage, m1=mid1_marks+"/"+mid1_weightage,m2=mid2_marks+"/"+mid2_weightage;
        String a=assignments_marks+"/"+assignments_weightage;
        finalMarks.setText(f);  projectMarks.setText(p);    mid1Marks.setText(m1);  mid2Marks.setText(m2);  assignMarks.setText(a);
    }

    public void goBack(View v){
        finish();
    }
}

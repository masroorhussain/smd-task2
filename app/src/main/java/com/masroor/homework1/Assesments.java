package com.masroor.homework1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Assesments extends AppCompatActivity {

    EditText a1Marks,a1Total,a2Marks,a2Total,a3Marks,a3Total,projMarks,projTotal,m1Marks,m1Total,m2Marks,m2Total,finalMarks,finalTotal;

    int a1Marks_,a1Total_,a2Marks_,a2Total_,a3Marks_,a3Total_,projMarks_,projTotal_,m1Marks_,m1Total_,m2Marks_,m2Total_,finalMarks_,finalTotal_;

    int w_f,w_p,w_m1,w_m2,w_a;  //weightages
    float m_f,m_p,m_m1,m_m2,m_a;  //absolute marks

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assesments);

        Intent rcvdIntent=getIntent();
        w_p=(int)rcvdIntent.getExtras().get(MainActivity.PROJECT_WEIGHTAGE);    w_f=(int)rcvdIntent.getExtras().get(MainActivity.FINAL_WEIGHTAGE);
        w_m1=(int)rcvdIntent.getExtras().get(MainActivity.MID1_WEIGHTAGE);      w_m2=(int)rcvdIntent.getExtras().get(MainActivity.MID2_WEIGHTAGE);
        w_a=(int)rcvdIntent.getExtras().get(MainActivity.ASSIGNMENTS_WEIGHTAGE);

        a1Marks=findViewById(R.id.a1Marks);a2Marks=findViewById(R.id.a2Marks);a3Marks=findViewById(R.id.a3Marks);
        a1Total=findViewById(R.id.a1Total);a2Total=findViewById(R.id.a2Total);a3Total=findViewById(R.id.a3Total);
        projMarks=findViewById(R.id.projectMarks);projTotal=findViewById(R.id.projTotal);
        m1Marks=findViewById(R.id.mid1Marks);m2Marks=findViewById(R.id.mid2Marks);
        m1Total=findViewById(R.id.m1Total);m2Total=findViewById(R.id.m2Total);
        finalMarks=findViewById(R.id.finalMarks);finalTotal=findViewById(R.id.finalTotal);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        getMarks();
        outState.putFloat(MainActivity.FINAL_MARKS,m_f);  outState.putFloat(MainActivity.PROJECT_MARKS,m_p);
        outState.putFloat(MainActivity.MID1_MARKS,m_m1);  outState.putFloat(MainActivity.MID2_MARKS,m_m2);
        outState.putFloat(MainActivity.ASSIGNMENTS_WEIGHTAGE,m_a);
    }
//
    @Override
    protected void onRestoreInstanceState(Bundle in) {
        super.onRestoreInstanceState(in);
        m_f=in.getFloat(MainActivity.FINAL_MARKS);  m_p=in.getFloat(MainActivity.PROJECT_MARKS);
        m_m1=in.getFloat(MainActivity.MID1_MARKS);  m_m2=in.getFloat(MainActivity.MID2_MARKS);
        m_a=in.getFloat(MainActivity.ASSIGNMENTS_WEIGHTAGE);
    }

    public boolean getMarks(){

        boolean done=false;

        String a1_marks_str=a1Marks.getText().toString(),a2_marks_str=a2Marks.getText().toString(),a3_marks_str=a3Marks.getText().toString();
        String a1_total_str=a1Total.getText().toString(),a2_total_str=a2Total.getText().toString(),a3_total_str=a3Total.getText().toString();

        String m1_marks_str=a1Marks.getText().toString(),m2_marks_str=m2Marks.getText().toString();
        String m1_total_str=m1Total.getText().toString(),m2_total_str=m2Total.getText().toString();

        String f_marks_str=finalMarks.getText().toString();
        String f_total_str=finalTotal.getText().toString();

        String p_marks_str=projMarks.getText().toString();
        String p_total_str=projTotal.getText().toString();

        if(
                !a1_marks_str.isEmpty() && !a2_marks_str.isEmpty() && !a3_marks_str.isEmpty()
                    &&
                !a1_total_str.isEmpty() && !a2_total_str.isEmpty() && !a3_total_str.isEmpty()
                    &&
                !m1_marks_str.isEmpty() && !m2_marks_str.isEmpty()
                    &&
                !m1_total_str.isEmpty() && !m2_total_str.isEmpty()
                    &&
                !f_marks_str.isEmpty()  && !f_total_str.isEmpty()
                    &&
                !p_marks_str.isEmpty()  && !p_total_str.isEmpty()
            )
        {
            a1Marks_=Integer.parseInt(a1_marks_str);    a1Total_=Integer.parseInt(a1_total_str);
            a2Marks_=Integer.parseInt(a2_marks_str);    a2Total_=Integer.parseInt(a2_total_str);
            a3Marks_=Integer.parseInt(a3_marks_str);        a3Total_=Integer.parseInt(a3_total_str);
            projMarks_=Integer.parseInt(p_marks_str);    projTotal_=Integer.parseInt(p_total_str);
            m1Marks_=Integer.parseInt(m1_marks_str);    m1Total_=Integer.parseInt(m1_total_str);
            m2Marks_=Integer.parseInt(m2_marks_str);    m2Total_=Integer.parseInt(m2_total_str);
            finalMarks_=Integer.parseInt(f_marks_str);  finalTotal_=Integer.parseInt(f_total_str);

            //calculate the absolutes
            m_f=(float)(finalMarks_*w_f)/finalTotal_;
            m_p=(float)(projMarks_*w_p)/projTotal_;
            m_m1=(float)(m1Marks_*w_m1)/m1Total_;
            m_m2=(float)(m2Marks_*w_m2)/m2Total_;
            m_a=(float)((a1Marks_+a2Marks_+a3Marks_)/(a1Total_+a2Total_+a3Total_)*w_a);

            Log.i("absolutes",""+m_f+","+m_p+","+m_m1+","+m_m2+","+m_a);

            done=true;
            return done;

        }else{
            Toast.makeText(this,"Please fill all details first.",Toast.LENGTH_SHORT).show();
        }
        return done;
    }

    public void processIt(View v){
        if(getMarks()){
            Intent returnIntent=new Intent();
            //place the calculated absolute marks in the returning intent
            returnIntent.putExtra(MainActivity.FINAL_MARKS,m_f);
            returnIntent.putExtra(MainActivity.PROJECT_MARKS,m_p);
            returnIntent.putExtra(MainActivity.MID1_MARKS,m_m1);
            returnIntent.putExtra(MainActivity.MID2_MARKS,m_m2);
            returnIntent.putExtra(MainActivity.ASSIGNMENTS_MARKS,m_a);
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
        }
    }
}
